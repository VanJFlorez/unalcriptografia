## How to compile and link:
    $ g++ DES.cc DES.h DES_main.cc DESkeys.cc DESkeys.h -o DES_main
    $ g++ DESinv_main.cc DESinv.h DESinv.cc DESkeys.h DESkeys.cc DES.h DES.cc -o DESinv_main

 g++ version: gcc version 6.3.0 20170516

### EXAMPLES:
    # Encryption
        * DES_main [key] [plaintext]
            $ ./DES_main "alfa beta gamma" "quoi de neuf?"
            > CYPHERTEXT:
            > 110011001101001111000000011010001001000111000100100110001000010.....

        * DES_main
            $ ./DES_main
            > message:
            > toc toc
            > key:
            > deux trois

            CYPHERTEXT:
            > 0100001011001001111000011000001010111010001000100011000011001010


    # Decryption
        * DESinv [key] [cyphertext]
            $ ./DESinv_main "deux trois" 0100001011001001111000011000001010111010001000100011000011001010
            > PLAIN TEXT:
            > toc toc


        * DESinv [key]
            $ ./DESinv_main "alfa beta gamma"
            > PLAIN TEXT:
            > quoi de neuf?

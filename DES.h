#include<string>
#include<bitset>
#include<vector>

using namespace std;

vector<bitset<8>> get_demo_message(string bit_string);
string bits_to_string(vector<bitset<64>> bits);
void write_to_file(vector<bitset<64>> bin);
bitset<64> msg_permutation(bitset<64> msg);
pair<bitset<32>, bitset<32>> split_msg_LR(bitset<64> msg);
bitset<48> expand_32_to_48(bitset<32> blk);
bitset<32> round_function(bitset<48> key, bitset<32> half);
vector<bitset<4>> compute_s_box(vector<bitset<6>> subblocks);
bitset<32> concatenate_s_box_output(vector<bitset<4>> sb_out);
bitset<32> s_box_permutation(bitset<32> sb_concat);
bitset<64> concatenate_after_16_rounds(bitset<32> R, bitset<32> L);
bitset<64> last_permutation(bitset<64> final_concatenation);

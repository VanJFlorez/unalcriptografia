#include<iostream>
#include<string>
#include<bitset>
#include<vector>
#include<algorithm>
#include<assert.h>
#include<fstream>
#include"DES.h"
#include"DESkeys.h"

// each char in str is converted to bitset
vector<bitset<8>> str_to_bin_vector(string str) {
    vector<bitset<8>> bin_values;
    for(size_t i = 0; i < str.size(); i++) {
        bitset<8> binchar(str.c_str()[i]); // the conversion does not invert the order of bits e.g. A = 0110 0001 and after bitset translation remains 0110 0001
        bitset<8> bin_reversed;
        bin_reversed[7] = binchar[0];
        bin_reversed[6] = binchar[1];
        bin_reversed[5] = binchar[2];
        bin_reversed[4] = binchar[3];
        bin_reversed[3] = binchar[4];
        bin_reversed[2] = binchar[5];
        bin_reversed[1] = binchar[6];
        bin_reversed[0] = binchar[7];
        bin_values.push_back(binchar);
    }
    return bin_values;
}

// Note everything goes from right to left
// concatenate bitsets in blocks of 64 bits
vector<bitset<64>> concatenate_bitsets(vector<bitset<8>> binaries) {
    vector<bitset<64>> blocks;
    bitset<64> concatenation;

    vector<bitset<8>>::iterator it = binaries.begin();
    int concat_ptr = 0;
    while(it != binaries.end()){
        if (concat_ptr == 64) {
            blocks.push_back(concatenation);
            concat_ptr = 0;
            concatenation.reset();
        }
        for(int k = 0; k < 8; k++, concat_ptr++)
            concatenation.set(concat_ptr, (*it)[k]);// bitset indexing starts from right
        it++;
    }
    blocks.push_back(concatenation);
    return blocks;
}

void print_bitset_vector(vector<bitset<8>> binaries) {
    for(size_t i = 0; i < binaries.size(); i++) {
        cout << binaries[i] << " ";
    }
    cout << endl;
}

void print_bitset_vector64(vector<bitset<64>> binaries) {
    for(size_t i = 0; i < binaries.size(); i++) {
        cout << binaries[i] << " ";
    }
    cout << endl;
}

/**
 * print a vector of bit chunks
 */
void serialize(vector<bitset<8>> v) {
    for(vector<bitset<8>>::iterator it = v.begin(); it < v.end(); it++) {
        for(int i = 0; i < 8; i++)
            cout << (*it)[i];
    }
    cout << endl;
}

void serialize64(vector<bitset<64>> v) {
    for(vector<bitset<64>>::iterator it = v.begin(); it < v.end(); it++) {
        for(int i = 0; i < 64; i++)
            cout << (*it)[i];
    }
    cout << endl;
}

/**************************************************************
 * Step 1: Create 16 subkeys, each of which is 48-bits long.  *
 **************************************************************/
// Note everything goes from right to left
// Step 1.1 Initial permutation of the key
bitset<56> key_permutation(bitset<64> key) {
    bitset<56> pkey;
    int perm_table[KEY_PERM_TABLE_SZ] = {57, 49, 41, 33, 25, 17,  9,
                                          1, 58, 50, 42, 34, 26, 18,
                                         10,  2, 59, 51, 43, 35, 27,
                                         19, 11,  3, 60, 52, 44, 36,
                                         63, 55, 47, 39, 31, 23, 15,
                                          7, 62, 54, 46, 38, 30, 22,
                                         14,  6, 61, 53, 45, 37, 29,
                                         21, 13,  5, 28, 20, 12, 4}; // idx = {i | i = 1 ... 64}
    for(int i = 0; i < KEY_PERM_TABLE_SZ; i++)
        pkey[i] = key[perm_table[i] - 1];

    /* print test
    cout << "i p k" << endl;
    for(int i = 0; i < 56; i++)
        cout << (i + 1) << " " << pkey[i] << " " << key[i] << " | k[" << perm_table[i] << "] = " << key[perm_table[i] - 1] << endl;
    */
    return pkey;
}

// generate the 28 pair of subkeys from the first permutation of the key.
vector<pair<bitset<28>, bitset<28>>> generate_subkeys(bitset<56> k_plus) {
    unsigned schedule[16] = {1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1};

    vector<pair<bitset<28>, bitset<28>>> subkeys;
    // split the key
    bitset<28> C;
    bitset<28> D;
    for(int i = 0; i < 28; i++)
        C[i] = k_plus[i];
    for(int i = 0; i < 28; i++)
        D[i] = k_plus[i + 28];
    // subkeys.push_back(pair<bitset<28>, bitset<28>>(C, D)); // in DES the intial split is not used!!!!
    // append shifted subkeys
    for(int i = 0; i < 16; i++) {
        C = shift_left(C, schedule[i]);
        D = shift_left(D, schedule[i]);
        subkeys.push_back(pair<bitset<28>, bitset<28>>(C,D));
        /* print test
        cout << "C: " << subkeys[i].first << " D: " << subkeys[i].second << endl;
        */
    }
    return subkeys;
}

// shifts to the left the given bitset pos times, as DES shiftS to the left and
// bitsets are inverted, here we shift to the RIGHT!
bitset<28> shift_left(bitset<28> half_key, size_t pos) {
    int tmp = 0;
    for(int i = 0; i < pos; i++) {
        tmp = half_key[0];
        half_key >>= 1;
        half_key.set(27, tmp);
    }
    return half_key;
}

// concatenate the C_i D_i subkeys generated by above function
vector<bitset<56>> concatenate_subkeys(vector<pair<bitset<28>, bitset<28>>> subkeys) {
    vector<bitset<56>> keys;
    bitset<56> concat;
    auto it = subkeys.begin();
    while(it != subkeys.end()) {
        for(int i = 0; i < 28; i++)
            concat.set(i, (*it).first[i]);
        for(int i = 0; i < 28; i++)
            concat.set(i + 28, (*it).second[i]);
        it++;
        keys.push_back(concat);
        /* print test
        cout << "C + D: " << concat << endl;
        */
    }
    return keys;
}

vector<bitset<48>> subkey_permutation(vector<bitset<56>> subkeys) {
    size_t perm_table[48] = { 14,   17,  11,   24,    1,   5,
                               3,   28,  15,    6,   21,  10,
                              23,   19,  12,    4,   26,   8,
                              16,    7,  27,   20,   13,   2,
                              41,   52,  31,   37,   47,  55,
                              30,   40,  51,   45,   33,  48,
                              44,   49,  39,   56,   34,  53,
                              46,   42,  50,   36,   29,  32 }; // indexing starts from 1
    vector<bitset<48>> permutated_keys;
    bitset<48> permutation;
    for(int i = 0; i < subkeys.size(); i++) {
        permutation.reset();
        for(int j = 0; j < 48; j++)
            permutation.set(j, subkeys[i][perm_table[j] - 1]);
        permutated_keys.push_back(permutation);
        /* print test
        cout << permutation << endl;
        */
    }
    return permutated_keys;
}

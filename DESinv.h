#include<string>
#include<bitset>
#include<vector>
#include<iostream>
using namespace std;

vector<bitset<8>> bin_string_to_byte_vector(string bits);

bitset<64> UNDO_last_permutation(bitset<64> final_concatenation);
pair<bitset<32>, bitset<32>> UNDO_concatenate_after_16_rounds(bitset<64> concat);
pair<bitset<48>, bitset<32>> UNDO_round_function(bitset<32> half);
bitset<32> UNDO_s_box_permutation(bitset<32> sb_concat);
vector<bitset<4>> UNDO_concatenate_s_box_output(bitset<32> concat); // not used due to Feistel Networks
bitset<32> UNDO_s_box_permutation(bitset<32> permutation);          // not used due to Feistel Networks
bitset<64> UNDO_split_msg_LR(bitset<32> L, bitset<32> R);
bitset<64> UNDO_msg_permutation(bitset<64> perm);

/**
 * prints bitset from left to right (usual order).
 * @param bits {bitset} - bits
 * @chunk_size {int} - each chunk_size bits a space is printed
 */
template<size_t N>
void print_bitset_left_to_right(bitset<N> bits, int chunk_size) {
    for(size_t i = 0; i < N; i++) {
        cout << bits[i];
        if ((i + 1)%chunk_size == 0)
            cout << " ";
    }
    cout << endl;
}

#include<iostream>
#include<string>
#include<bitset>
#include<vector>
#include<algorithm>
#include<assert.h>
#include<fstream>
#include"DESinv.h"

using namespace std;

// this function is equal to "vector<bitset<8>> get_demo_message(string bits);" in DES.cc
vector<bitset<8>> bin_string_to_byte_vector(string bits) {
    string demo_msg = bits;

    vector<bitset<8>> msg;

    string str_chunk = "";
    for(int i = 0; i < demo_msg.size(); i++) {
        str_chunk += demo_msg[i];
        if((i +  1)%8 == 0) {
            reverse(str_chunk.begin(), str_chunk.end());
            bitset<8> bit_chunk(str_chunk);
            msg.push_back(bit_chunk);
            str_chunk = "";
            bit_chunk.reset();
        }
    }
    cout << endl;
    return msg;
}



// undoes the final concatenation got after 16 rounds. pair.first = L and pair.second.R
pair<bitset<32>, bitset<32>> UNDO_concatenate_after_16_rounds(bitset<64> concat) {
    bitset<32> L, R;
    for(int i = 0; i < 32; i++)
        R.set(i, concat[i]);
    for(int i = 0; i < 32; i++)
        L.set(i, concat[i + 32]);

    return pair<bitset<32>, bitset<32>> (L, R);
}

// inverse of last_permutation() in DES.cc
bitset<64> UNDO_last_permutation(bitset<64> permutation) {
    const int perm_table[64] = {
                    40,    8,  48,   16,   56,  24,   64,  32,
                    39,    7,  47,   15,   55,  23,   63,  31,
                    38,    6,  46,   14,   54,  22,   62,  30,
                    37,    5,  45,   13,   53,  21,   61,  29,
                    36,    4,  44,   12,   52,  20,   60,  28,
                    35,    3,  43,   11,   51,  19,   59,  27,
                    34,    2,  42,   10,   50,  18,   58,  26,
                    33,    1,  41,    9,   49,  17,   57,  25
    };
    bitset<64> undo_perm;
    for(int i = 0; i < 64; i++)
        undo_perm.set(perm_table[i] - 1, permutation[i]);
    /* print test
    cout << final_permutation << endl;
    */
    return undo_perm;
}

// This is the UNDO_f function that operates inside of each loop. This function reverses round_function() in DES.cc
pair<bitset<48>, bitset<32>> UNDO_round_function(bitset<32> half) {

    return pair<bitset<48>, bitset<32>> ();
}

// UNDO the s-box permutation() made by s_box_permutation() in DES.cc
bitset<32> UNDO_s_box_permutation(bitset<32> permutation) {
    size_t perm_table[32] = {
        16,  7, 20, 21,
        29, 12, 28, 17,
         1, 15, 23, 26,
         5, 18, 31, 10,
         2,  8, 24, 14,
        32, 27,  3,  9,
        19, 13, 30,  6,
        22, 11,  4, 25,
    };
    bitset<32> undo_perm;
    for(size_t i = 0; i < 32; i++)
        undo_perm.set(perm_table[i] - 1, permutation[i]);
    return undo_perm;
}

// undo concatenation of S-box transformation in concatenate_s_box_output() in DES.cc
vector<bitset<4>> UNDO_concatenate_s_box_output(bitset<32> concat) {
    vector<bitset<4>> blocks;
    bitset<4> blk;
    for(int i = 0, j = 0; i < 32; i++, j++) {
        blk.set(j, concat[i]);
        if((j + 1)%4 == 0) {
            j = -1;
            blocks.push_back(blk);
            cout << blk << endl;
            blk.reset();
        }
    }
    return blocks;
}

// Here we concatenate the split made by split_msg_LR(...) in DES.cc
bitset<64> UNDO_split_msg_LR(bitset<32> L, bitset<32>R) {
    bitset<64> concat;
    for(int i = 0; i < 32; i++)
        concat[i] = L[i];
    for(int i = 0; i < 32; i++)
        concat[i + 32] = R[i];
    return concat;
}


// Undo the initial permutation of message block made by msg_permutation(...) in DES.cc
bitset<64> UNDO_msg_permutation(bitset<64> perm) {
    size_t perm_table[64] = { 58,   50,  42,   34,   26,  18,   10,   2,
                              60,   52,  44,   36,   28,  20,   12,   4,
                              62,   54,  46,   38,   30,  22,   14,   6,
                              64,   56,  48,   40,   32,  24,   16,   8,
                              57,   49,  41,   33,   25,  17,    9,   1,
                              59,   51,  43,   35,   27,  19,   11,   3,
                              61,   53,  45,   37,   29,  21,   13,   5,
                              63,   55,  47,   39,   31,  23,   15,   7};
    bitset<64> msg;
    for(int i = 0; i < 64; i++)
        msg[perm_table[i] - 1] = perm[i];
    /* print test
    cout << perm_msg << endl;
    */
    return msg;
}

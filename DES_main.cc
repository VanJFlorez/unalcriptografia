#include<iostream>
#include<string>
#include<bitset>
#include<vector>
#include<algorithm>
#include<assert.h>
#include<fstream>
#include"DES.h"
#include"DESkeys.h"

bool TEST = false;

int main(int argc, char *argv[]) {
    // All output is reversed
    vector<bitset<8>> bin_message;
    vector<bitset<8>> bin_key;
    if(!TEST) {
        string message;
        string key;
        if (argc == 1) {
            cout <<  "message: " << endl;
            getline(cin, message);
            cout << "key: " << endl;
            getline(cin, key);
        } else {
            message = argv[2];
            key = argv[1];
        }
        bin_message = str_to_bin_vector(message);
        bin_key = str_to_bin_vector(key);
    } else {
        bin_message = get_demo_message("0000000100100011010001010110011110001001101010111100110111101111");
        bin_key = get_demo_message("0001001100110100010101110111100110011011101111001101111111110001");
    }
    /* print test
    cout << "msg: ";
    serialize(bin_message);
    cout << endl;
    cout << "key: ";
    serialize(bin_key);
    cout << endl;
    */

    // Step 1: Create 16 subkeys, each of which is 48-bits long.
    vector<bitset<64>> kblocks = concatenate_bitsets(bin_key);
    bitset<56> k_plus = key_permutation(kblocks[0]); // only use the first 64 of the key...
    vector<pair<bitset<28>, bitset<28>>> subkeys = generate_subkeys(k_plus);
    vector<bitset<56>> keys = concatenate_subkeys(subkeys);
    vector<bitset<48>> subkeys_permutated = subkey_permutation(keys); // These are the keys that will be used in next step

    // Step 2: Encode each 64-bit block of data.
    vector<bitset<64>> msg_blocks = concatenate_bitsets(bin_message);
    vector<bitset<64>> encrypted_blocks;
    for(int i = 0; i < msg_blocks.size(); i++) {
        bitset<64> msg_perm = msg_permutation(msg_blocks[i]);
        // 16 rounds
        pair<bitset<32>, bitset<32>> LR = split_msg_LR(msg_perm);
        bitset<32> L = LR.first;
        bitset<32> R = LR.second;
        for(int i = 0; i < 16; i++) {
            bitset<32> tmp_L = L;
            /* print test
            cout << "in: L = " << L << " R = " << R << " K = " << subkeys_permutated[i] << endl;
            */
            L = R;
            R = tmp_L^round_function(subkeys_permutated[i], R);
            /* print test
            cout << "ou: L = " << L << " R = " << R << endl;
            */
        }
        bitset<64> final_concatenation = concatenate_after_16_rounds(R, L); // note that in this concatenation we put first R and then L
        bitset<64> encrypted_blk = last_permutation(final_concatenation);

        encrypted_blocks.push_back(encrypted_blk);
    }
    string chars = bits_to_string(encrypted_blocks); // Mostly of the chars are non-printable, so view the output file
    /*
    cout << chars << endl;
    cout << " *! Mostly of the chars are non-printable, so review the output file." << endl;
    */
    cout << "\nCYPHERTEXT: " << endl;
    write_to_file(encrypted_blocks);
    return 0;
}

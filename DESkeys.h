#include<string>
#include<bitset>
#include<vector>

#define KEY_SZ 64
#define KEY_PERM_TABLE_SZ 56

using namespace std;

vector<bitset<8>> str_to_bin_vector(string str);
vector<bitset<64>> concatenate_bitsets(vector<bitset<8>> binaries);
void print_bitset_vector(vector<bitset<8>>);
void print_bitset_vector64(vector<bitset<64>>);
void serialize(vector<bitset<8>> v);
bitset<56> key_permutation(bitset<64> key);
vector<pair<bitset<28>, bitset<28>>> generate_subkeys(bitset<56> k_plus);
bitset<28> shift_left(bitset<28> half_key, size_t pos);
vector<bitset<56>> concatenate_subkeys(vector<pair<bitset<28>, bitset<28>>> subkeys);
vector<bitset<48>> subkey_permutation(vector<bitset<56>> subkeys);

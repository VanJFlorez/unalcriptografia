#include<iostream>
#include<string>
#include<bitset>
#include<vector>
#include<algorithm>
#include<assert.h>
#include<fstream>
#include"DESinv.h"
#include"DESkeys.h"
#include"DES.h" // for get_demo_message(..)

const bool TEST = false;

int main(int argc, char *argv[]) {
    vector<bitset<8>> bin_key;
    vector<bitset<8>> bin_cyphertext;
    ifstream file;
    if(!TEST) {
        string cyphertext;
        string key;
        if (argc == 1) {
            cout <<  "cyphertext: " << endl;
            getline(cin, cyphertext);
            cout << "key: " << endl;
            getline(cin, cyphertext);
            bin_cyphertext = str_to_bin_vector(cyphertext);
        } else if (argc == 2) {
            file.open("cyphertext.txt");
            string filestream((istreambuf_iterator<char>(file)), (istreambuf_iterator<char>()));
            // reverse(filestream.begin(), filestream.end()); // why?
            key = argv[1];
            bin_cyphertext = bin_string_to_byte_vector(filestream);
        } else {
            cyphertext = argv[2];
            key = argv[1];
            bin_cyphertext = get_demo_message(cyphertext);
        }
        bin_key = str_to_bin_vector(key);
    } else {
        bin_key = get_demo_message("0001001100110100010101110111100110011011101111001101111111110001");
        bin_cyphertext = get_demo_message("1000010111101000000100110101010000001111000010101011010000000101");
    }
    /* print test
    cout << "cry: ";
    serialize(bin_cyphertext);
    cout << endl;
    cout << "key: ";
    serialize(bin_key);
    cout << endl;
    */

    // Step 1: Create 16 subkeys, each of which is 48-bits long.
    vector<bitset<64>> kblocks = concatenate_bitsets(bin_key);
    bitset<56> k_plus = key_permutation(kblocks[0]); // only use the first 64 of the key...
    vector<pair<bitset<28>, bitset<28>>> subkeys = generate_subkeys(k_plus);
    vector<bitset<56>> keys = concatenate_subkeys(subkeys);
    vector<bitset<48>> subkeys_permutated = subkey_permutation(keys); // These are the keys that will be used in next step

    vector<bitset<64>> encrypted_blocks = concatenate_bitsets(bin_cyphertext);
    vector<bitset<64>> msg_blocks;
    for(vector<bitset<64>>::iterator iter = encrypted_blocks.begin(); iter < encrypted_blocks.end(); iter++) {
        bitset<64> undo = UNDO_last_permutation(*iter);
        pair<bitset<32>, bitset<32>> L_R = UNDO_concatenate_after_16_rounds(undo);

        bitset<32> L = L_R.first;
        bitset<32> R = L_R.second;
        // 16 rounds
        for(int i = 0; i < 16; i++) {
            bitset<32> tmp_L = L;
            /* print test
            cout << "in: L = " << L << " R = " << R << " K = " << subkeys_permutated[15 - i] << endl;
            */
            L = R^round_function(subkeys_permutated[15 - i], L);
            R = tmp_L;
            /* print test
            cout << "ou: L = " << L << " R = " << R << endl;
            */
        }
        bitset<32> L_0 = L;
        bitset<32> R_0 = R;

        bitset<64> perm_mesagge = UNDO_split_msg_LR(L_0, R_0);
        bitset<64> message = UNDO_msg_permutation(perm_mesagge);
        msg_blocks.push_back(message);
    }
    string decrypted_message = bits_to_string(msg_blocks);
    cout << "PLAIN TEXT:\n" << decrypted_message << endl;
    return 0;
}
